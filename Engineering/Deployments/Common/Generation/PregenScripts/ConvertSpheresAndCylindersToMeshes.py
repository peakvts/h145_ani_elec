# This file is licensed under the MIT License.
# 
# Copyright (c) 2016 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
A pregeneration script that converts spheres and cylinders, which are not suported on 
GL Studio ES, to meshes. It only runs when it finds spheres or cylinders in the given file.

It can be used as a template for custom pregeneration scripts to perform object conversion,
texture atlasing, tessellation, or other functional or optimization operations. 

Pregeneration scripts need to have one function -- Pregen( doc ) -- that accepts a GLS
file as a parameter. Such scripts can alter the file at will because the build system saves 
and then restores a backup of the GLS file when running the given set of pregeneration scripts.

Optionally, pregeneration scripts can supply the NeedsPregen() function to allow the
script to indicate whether it should run or not. This speeds up the build process since copying,
modifying, saving, generating, and reloading can take a significant amount of time depending
on the contents of the GLS file. (Note, reloading also loses the editor's undo history for the
file, so this is another reason to supply this function with each pregeneration script.)
"""

import DisplayObject
import Document
import Editor
import Group
import Catalyzer


# This is an optional function to quickly identify if this script should run.
# this could query for a configuration object in the gls file that disables
# certain optimizations for a file or reference a lookup table.
# in this example, the script determines if any objects in this gls files can 
# benefit from this script running
def NeedsPregen( doc ):
    doc.FindInDocument(None, "GlsSphere", None, None, 1, True, False, False)
    if doc.GetSelectedObjects().GetCount() > 0:
        return True
        
    doc.FindInDocument(None, "GlsCylinder", None, None, 1, True, False, False)
    if doc.GetSelectedObjects().GetCount() > 0:
        return True
        
    return False


# Pregenerate function, put code to run before generating source, such as 
# automated optimizations.  This example makes files compatible with the 
# ES runtime by converting the unsupported sphere and cylinder objects into 
# tri-mesh objects.
def Pregen( doc ):
    print "Running PreGenerate on ",doc.GetFilename()

    ConvertCatalyzersToGroups(doc)

    # Find all Spheres and Cylinders convert to trimesh 
    doc.FindInDocument(None, "GlsSphere", None, None, 1, True, False, False)
    spheres = doc.GetSelectedObjects()
    
    doc.FindInDocument(None, "GlsCylinder", None, None, 1, True, False, False)
    cylinders = doc.GetSelectedObjects()
    
    # group the cylinders and spheres
    doc.SelectObjects(spheres)
    selectedObjectArray = doc.GetSelectedObjects()
    for i in range(selectedObjectArray.GetCount()):
        obj = selectedObjectArray.GetObjectByIndex(i)
        doc.UnselectObjects(doc.GetSelectedObjects())
        doc.SelectObject(obj)
        doc.ConvertSelectedToMesh()


# Catalyzers will not let you modify hierarchy, this converts them to groups
def ConvertCatalyzersToGroups( doc ):
    doc.FindInDocument(None, "Group", None, None, 1, True, False, False)
    groups = doc.GetSelectedObjects()
    doc.UnselectObjects(doc.GetSelectedObjects())
    
    for i in range(groups.GetCount()):
        g = Group.Group.CastToGroup(groups.GetObjectByIndex(i))
        RecursivelyFindAndReplaceCatalyzers(doc, g)


def ReplaceCatalyzer( doc, g ):
    gname = g.GetName()
    itemsInGroup = g.GetObjectList()
    newGroup = Group.Group( )
    doc.InsertObject(newGroup)
    for a in range(itemsInGroup.GetCount()):
        newGroup.MoveObjectToGroup(itemsInGroup.GetObjectByIndex(a), True, -1, False)
    g.Ungroup()
    newGroup.SetName(gname)
    return newGroup


# pass in all groups, replace catalyzers, recurse on groups
def RecursivelyFindAndReplaceCatalyzers( doc, g ):
    itemsInGroup = g.GetObjectList()
    for a in range(itemsInGroup.GetCount()):
        obj = itemsInGroup.GetObjectByIndex(a)
        if( IsAGroup(obj) ):
            if( IsACatalyzer(obj) ):
               ReplaceCatalyzer(doc, Group.CastToGroup(obj))
            else:
                RecursivelyFindAndReplaceCatalyzers(doc, Group.CastToGroup(obj))


def IsACatalyzer( object ):
    catalyzerObject = Catalyzer.CastToCatalyzer( object )
    if catalyzerObject:
        return True
    else:
        return False


def IsAGroup( object ):
    grpobj = Group.CastToGroup( object )
    if grpobj:
        return True
    else:
        return False
