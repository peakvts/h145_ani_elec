# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Creates a SCons Environment suitable for creating a Desktop Linux executable """

import os
import glob
import sys
import shutil
import SCons.Script
import SCons.Environment
import platform
import commands
import json

def FindGLSSupportedGCCVersions():
    # file names end with _gcc_###.so
    glsLibs = glob.glob(os.environ['GLSTUDIO']+'/lib/libglstudioLinux*_gcc_*.so')
    if '64bit' in platform.architecture():
        versions = [ x[-6:-3] for x in glsLibs if 'Linux64' in x ]
    else:
        versions = [ x[-6:-3] for x in glsLibs if 'Linux64' not in x ]
    return versions

def Environment( *args, **kwargs ):

    if not 'TARGET_ARCH' in kwargs:
        kwargs['TARGET_ARCH'] = 'x86'
    vars = SCons.Script.Variables()
    vars.AddVariables(
            SCons.Script.BoolVariable('DEBUG', 'Set to 0 to build for release', 1),
            );
    kwargs['variables'] = vars
    env = SCons.Script.Environment( *args, **kwargs )

    glsSupportedVersions = FindGLSSupportedGCCVersions()
    
    gccVersionDump = commands.getoutput(env['CXX'] + ' -dumpversion')
    gccVer = 100 * int(gccVersionDump.split('.')[0]) + 10 * int(gccVersionDump.split('.')[1])

    if str(gccVer) not in glsSupportedVersions:
        print 'libraries for GCC ' + str(gccVer) + ' are not currently installed'
        quit()

    env.Append(CCFLAGS = [
        '-DLINUX' 
        ,'-I/usr/X11R6/include/'
        ,'-fPIC'
    ])

    if env['DEBUG']:
        env.Append(CCFLAGS = [
            '-D_DEBUG',
        ])
    else:
        env.Append(CCFLAGS = [
            '-DNDEBUG',
        ])

    origin = "'$$ORIGIN:" + str(os.environ['GLSTUDIO']) + "/lib'"
    env.Append( RPATH = env.Literal(origin) )

    env.Append(LINKFLAGS = [
        
    ])
    if env['DEBUG']:
        env.Append(LINKFLAGS = [
            '-g',
        ])

    env.Append(CPPPATH = [
        os.environ['GLSTUDIO'] + '/include'
        ,os.environ['GLSTUDIO'] + '/plugins/include'
    ])

    env.Append(LIBPATH = [
        os.environ['GLSTUDIO'] + '/plugins/lib'
        ,os.environ['GLSTUDIO'] + '/lib'
    ])

    suffix = 'Linux'
    if '64bit' in platform.architecture():
        suffix += '64'
    suffix += '_gcc_' + str(gccVer)

    plugins = glob.glob(os.environ['GLSTUDIO'] + '/plugins/lib/*.a')
    plugins = [ i.split('/')[-1] for i in plugins if suffix in i ]

    ctlPlugins = ['pango-1.0', 'pangoft2-1.0', 'glib-2.0', 'gthread-2.0', 'gmodule-2.0', 'gio-2.0', 'gobject-2.0']

    hasSupportForCTL = True
    conf = SCons.Script.Configure(env)
    for lib in ctlPlugins:
        if not conf.CheckLib( library=lib, autoadd=0 ):
            hasSupportForCTL = False
            break
    conf.Finish()    

    if hasSupportForCTL:
        plugins += ctlPlugins
    else:
        print "Warning.  Pango libraries not available on system so CTLTextBox will not be available."

    linkStaticGlsLib = False  # Set to True for static linking of the GL Studio library
        
    if linkStaticGlsLib:
        glstudioLibrary = 'libgls'+suffix
    else:
        glstudioLibrary = 'libglstudio'+suffix

    env.Append(
    LIBS=[
        glstudioLibrary
        ,plugins
        ,'GL'
        ,'X11'
        ,'GLU'
        ,'m'
        ,'pthread'
        ,'usb'
        ,'audiofile'
        ,'rt'
    ])
    
    # target must be a file name.  source must be an exe.
    def _CopyExeAndDependentGlsDlls( target, source, env ):
        # List GL Studio dependencies
        ldd = commands.getoutput('ldd ' + os.path.abspath(str(source[0])))
        # Extract libraries' filepaths from ldd output 
        # "glstudioLinux_gcc_460.so.5.1.1 => /home/glsbuild/glstudio/lib/glstudioLinux_gcc_460.so.5.1.1 (0xb7130000)"
        
        glStudioDirectory = os.path.realpath( os.environ['GLSTUDIO'] )
        referencedGLStudioSOs = []

        for x in ldd.splitlines():
            try:
                # Input of: "glstudioLinux_gcc_460.so.5.1.1 => /home/glsbuild/glstudio/lib/glstudioLinux_gcc_460.so.5.1.1 (0xb7130000)"
                fullFilePath = x.split(">",2)[1].rsplit(None,1)[0].strip()
                fullFilePath = os.path.realpath( fullFilePath )
                # Will result in "/home/glsbuild/glstudio/lib/glstudioLinux_gcc_460.so.5.1.1"
                # For each ldd entry, see if it is referring to a file in the GL Studio directory
                # We are comparing realpath with realpath, so string comparison should be valid.
                if fullFilePath.startswith( glStudioDirectory ) and os.path.exists(fullFilePath):
                    # Add it to the list
                    referencedGLStudioSOs.append( fullFilePath )
            except:
                # If the string parsing of a line fails, we didn't want it anyway, just move on to the next line
                pass

        # Support target being a directory
        targetPath = str(target[0])
        sourcePath = str(source[0])
        dllTargetDir = os.path.dirname( os.path.abspath(targetPath) )
        if os.path.isdir( targetPath ):
            # We are going to use the source name to determine the target name
            targetPath = os.path.join( dllTargetDir, os.path.basename( sourcePath ) )
        shutil.copy2( sourcePath, dllTargetDir )
        
        # Copy all GL Studio referenced .so files.
        for x in referencedGLStudioSOs:
            shutil.copy2( x , dllTargetDir )

    # destination must be a directory
    def _InstallWithGlsDLLs( destination, source, *args, **kwargs):
        if 'action' in kwargs:
            print "ERROR: 'action' already defined when calling InstallWithGlsDLLs.  It should be undefined."
        else:
            kwargs['action'] = _CopyExeAndDependentGlsDlls
            # Build the target exe from the source file into the destination directory
            target = os.path.abspath(os.path.join( destination, os.path.basename( str(source[0])) ))
            env.Command( target, source, *args, **kwargs) 
            
    # Allows for clean usage in the SConstruct along side other env.Install() calls.
    env.InstallWithGlsDLLs = _InstallWithGlsDLLs

    return env
