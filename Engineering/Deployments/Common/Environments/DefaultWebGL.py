# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Creates a SCons Environment suitable for creating a WebGL/Emscripten target"""

import os
import glob
import sys
import shutil
import SCons.Script
import SCons.Environment
import platform
import commands
import json
import Common.Constants

def Environment( *args, **kwargs ):
    intermediateBaseDir = 'intermediate'

    newArgs = { 'tools':['mingw'], 'CC':"emcc", 'CXX':"em++", 'LD':"em++", 'AR':"emar", 'PROGSUFFIX':".html", 'ENV':os.environ }
    
    # Overwrite with any provided arguments
    newArgs.update( kwargs )

    vars = SCons.Script.Variables()
    vars.AddVariables(
            SCons.Script.BoolVariable('DEBUG', 'Set to 0 to build for release', 1),
            );
    newArgs['variables'] = vars 
    
    env = SCons.Script.Environment( *args, **newArgs )
    
    # Specify the emcc cache to be in the intermediate directory so it will get cleaned.
    env.Append(LINKFLAGS=[
        "--cache", os.path.abspath( intermediateBaseDir + '/emcc_cache' ),
        ])

    env.Append(CPPPATH=[
        os.environ['GLSTUDIO'] + "/GLStudioWebGLRuntime/include/glstudio",
        ])

    env.Append(CCFLAGS= [
        "-Wno-warn-absolute-paths",
        "-DGLS_ANIMATION_USE_STD_SHARED_PTR", 
        "-DSHOW_RESOURCE_CONTROL_BUTTON",
        "-DNOSOUND",
        "-DGLES",
        "-DGLES20",
        "-DMATRIX_TYPE_FLOAT",
        "-DNO_EXIT_RUNTIME=1",
        "-DEMSCRIPTEN",
        "-D__STDC__",
        "-std=c++0x",
        "-MD",
        "-DF",
    ])
    if env['DEBUG']:
        env.Append(CCFLAGS= [
            "-DASSERTIONS=2",
            "-DSAFE_HEAP=1",
        ])
    
    env.Append(LINKFLAGS=[
        "-s","ERROR_ON_UNDEFINED_SYMBOLS=1", # So any link errors will cause the build to fail (rather than waiting until runtime to fail)
        "--emrun",                           # Turns on the stderr, stdout reporting back to the server
    ])

    env.Append(LIBS=[
        "GL",
        "GLU",
        env.File(os.environ['GLSTUDIO'] + "\\GLStudioWebGLRuntime\\lib\\glstudio\\libglstudio_emscripten_release.bc"),
    ])

    # Take a list of files (and directories) and stuff them all into the resources.js and resources.data files.  These files are interpreted at runtime to build up the "file system".
    def _PackageResources( distDir, resourcesDir, resourceFiles, additionalDirectories, additionalFiles = [] ):
    
        # Grab resources directory name from the first resources file
        additionalDirectories.append( os.path.abspath(resourcesDir) )
        
        # Build up the command line to --preload the files
        preloadToRoot  = [ '--preload "'+os.path.abspath(x)+'"@.' for x in additionalDirectories ]      
        preloadToRoot += [ '--preload "'+os.path.abspath(x)+'"@'+os.path.basename(x) for x in additionalFiles ]      
        preloadString = ' '.join(preloadToRoot)

        # Create the SCons command to create the resource files (if needed by dependency)
        env.Command([distDir+'/resources.data', distDir+'/resources.js'], 
                    # Define the source files, so dependencies are tracked
                    [os.environ['EMSCRIPTEN']+'/tools/file_packager.py', 
                     additionalFiles,
                     resourceFiles,
                    ],
                    ['python "'+os.environ["EMSCRIPTEN"]+'/tools/file_packager.py" "'+ distDir +'/resources.data"'
                    +' --exclude *.pvr *.crc'
                    +' --js-output="'+distDir+'/resources.js"'
                    +' --no-heap-copy'
                    +' '+preloadString
                    ])    
        
    env.PackageResources = _PackageResources
    
    def _CreateDefaultProgram( config, glsGeneratedFilesDir, sourceFiles, additionalRuntimeDirectories, additionalRuntimeFiles ):    
        resourceFiles = []

        generatedFiles = None
        with open( glsGeneratedFilesDir + "/" + Common.Constants.fileNameOfGeneratedFileList, "r") as f:
            generatedFiles = json.load( f )

        if generatedFiles:
            sourceFiles.extend( generatedFiles['SOURCE'] )
            resourceFiles.extend( generatedFiles['RESOURCE'] )
            # NOTE: We don't have to tell SCons about the header files because it will find the dependencies in the cpp files.

        distDir = 'dist'
        if env['DEBUG']:
            intermediateDir = intermediateBaseDir + '/Debug'
            distDir += '/Debug'
        else:    
            intermediateDir = intermediateBaseDir + '/Release'
            distDir += '/Release'
            

        env.Append(CPPPATH=[
            glsGeneratedFilesDir,
            config.alternateClassBehaviorDir,
            config.additionalSourceDir,
            ])

        if env['DEBUG']:
            env.Append(CCFLAGS= [
                "-g",               # Debug information
                "-O0",              # No optimizations
            ])
        else:
            env.Append(CCFLAGS= [
                "-O2",              # Recommended optimizations for release build
            ])


        env.Append(LINKFLAGS=[
            "--shell-file", "shell.html",        # Identifies the html page used as the starting point for the target html
            "-s","TOTAL_MEMORY=402653184",       # If your project runs out of memory at runtime, increase this value
        ])
        if env['DEBUG']:
            env.Append(LINKFLAGS=[
                "-g4",                               # Full debug info (view C++ code in browser.)
            ])
        else:
            env.Append(LINKFLAGS=[
                "-g0"                                # No debug information
            ])

        env.Append(LIBS=[
        ])

        env.PackageResources(
            distDir,
            glsGeneratedFilesDir+"/resources",  # Directory where the generated resources go 
            resourceFiles,                      # Known resource files, for dependency checking
            additionalRuntimeDirectories,
            additionalRuntimeFiles )
           
                    
        # Using VariantDir allows us to keep the temporary files out of the generated folder.
        # Instead they go to the 'temp' directory.
        env.VariantDir(intermediateDir+'/gls_generated', glsGeneratedFilesDir, duplicate=0)

        # Shuffle the directories of the source files to request them in the variant directories 
        absGeneratedDir = os.path.abspath(glsGeneratedFilesDir)
        absGeneratedVariantDir = os.path.abspath(intermediateDir+'/gls_generated')
        sourceFilesInVariantDirs = [os.path.abspath(w).replace(absGeneratedDir, absGeneratedVariantDir) for w in sourceFiles]
        env.VariantDir(intermediateDir+'/build', '.', duplicate=0)

        additionalSourceVariantDir = os.path.abspath(intermediateDir+'/additional_source')
        env.VariantDir(additionalSourceVariantDir, config.additionalSourceDir, duplicate=0)
        sourceFilesInVariantDirs = [os.path.abspath(w).replace(config.additionalSourceDir, additionalSourceVariantDir) for w in sourceFilesInVariantDirs]

        program = env.Program(source=[intermediateDir+'/build/webgl_main.cpp', sourceFilesInVariantDirs], target=distDir+'/index.html')
        env.Depends( program, 'shell.html' )

        ##############################################
        ## Creates a Visual Studio project for viewing the related source code.
        ## If you don't need a VS project, you can safely remove this section since 
        ## it is not required to perform the build.
        ##############################################

        # Create an MSVC environment, just so we can create a Visual Studio project
        msvcEnv = SCons.Script.Environment( TARGET_ARCH = 'x86' )
        buildSettings = {
            'LocalDebuggerCommand':'run.bat',
            'LocalDebuggerWorkingDirectory':'..',
        }

        if True:
            # If you want to ensure that all generated code is up to date when you build use this:
            msvcEnv['MSVSSCONSCOM'] = '..\\build.bat'  # This generates, and then calls scons.bat
        else:    
            # If you want to do quick builds while working on non-generated source, use this:
            msvcEnv['MSVSSCONSCOM'] = '..\\scons.bat'   

        possibleIncludes = \
            glob.glob(config.alternateClassBehaviorDir+'/*.h') +\
            glob.glob(config.additionalSourceDir+'/*.h')

        msvcEnv.MSVSProject(target = 'visualc/WebGLBuild' + msvcEnv['MSVSPROJECTSUFFIX'],
                        srcs = sourceFiles,
                        incs = possibleIncludes + generatedFiles['HEADER'],
                        buildtarget = program,
                        variant = 'Debug',
                        DebugSettings = buildSettings)

        ##############################################
                        
    env.CreateDefaultProgram = _CreateDefaultProgram
    
    
    return env
    
