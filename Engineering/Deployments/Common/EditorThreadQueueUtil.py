# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""  Simplifies threaded access to the Editor  """

import Editor
import EditorLog

def CallOnMainThread(func, *args, **keywords):
    def callIt():
        func(*args, **keywords)        
    Editor.mainThreadQueue.put(callIt)

def CallOnMainThreadAndWait(func, *args, **keywords):
    class local:
        rval = None
    def CallIt():
        local.rval = func(*args, **keywords)
        
    if not Editor.shuttingDownEvent.isSet():
        Editor.mainThreadQueue.put(CallIt)
        Editor.mainThreadQueue.join()
    return local.rval
    
    
def LogInfo(message):
    CallOnMainThread(EditorLog.PrintInfo, message)
    
def LogWarning(message):
    CallOnMainThread(EditorLog.PrintWarning, message)

def LogError(message):
    CallOnMainThread(EditorLog.PrintError, message)
