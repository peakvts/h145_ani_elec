# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Allows for starting and killing built programs """

import subprocess
import signal
import Common.EditorThreadQueueUtil as editor

existingRunProcess = None

existingRunProcessByType = dict()
           
class Runner():
    def __init__( self, runnerModule, command, arguments ):
        self.runnerModule = runnerModule
        self.command = command
        self.arguments = arguments
        
        if not self.runnerModule.TypeId or not self.runnerModule.Run:
            editor.LogError( "Runner type invalid" + __FILE__ )
        else:
            self.typeOfRun = self.runnerModule.TypeId()
        
    def TerminateExistingRunProcess(self):
        if not self.typeOfRun in existingRunProcessByType:
            return # Nothing to do here
            
        existingRunProcess = existingRunProcessByType[ self.typeOfRun ]
        if existingRunProcess:
            if None == existingRunProcess.returncode:
                # Process is still running, we are going to terminate it
                editor.LogInfo("Stopping previous process.")
                existingRunProcess.terminate()
                existingRunProcess.kill()
                editor.LogInfo("Waiting for process to end.")
                existingRunProcess.wait()
                editor.LogInfo("Process ended")
            existingRunProcessByType[ self.typeOfRun ] = None

    def Run( self ):
        self.runnerModule.Run( self )
        
