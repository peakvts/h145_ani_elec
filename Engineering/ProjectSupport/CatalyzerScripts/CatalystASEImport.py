# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Catalyst - Imports DSI/ASE files and updates when the file changes. """

from GlsASEImporter import GlsASEImporter
import EditorLog
import os
from catalystUtil import CatalyzerSourceChangeMonitor

def Catalyzer_GetPropertiesList():
    # [ ( internalName, propType, displayName, groupName, description, defaultValue ), ]
    return [
    ( "DSIFile",                      "utf8_filepath", "DSI/ASE File",              __name__, "The DSI/ASE file to import.",                                                ""    ), 
    ( "_DSIFile_LastUpdateSignature", "ascii",         "",                          __name__, "This hidden property must exist and the name must follow this pattern to be used by the FileSignatureSupport module", "" ),
    ( "RepositionOnImport",           "boolean",       "Reposition to bottom-left", __name__, "Move the geometry so it's bottom left extents are at 0,0.",                  False ), 
    ( "EnableLighting",               "boolean",       "Enable lighting",           __name__, "Enables the lighting flag on imported geometry.",                            True  ), 
    ( "SplitAnimations",              "boolean",       "Split animations",          __name__, "Split animations into individual components.",                               False ), 
    ( "IgnoreFileNormals",            "boolean",       "Ignore normals",            __name__, "Ignore the DSI file's normals.",                                             False ), 
    ( "ScaleFactor",                  "float32",       "Scale",                     __name__, "Scale the geometry by this factor after import. Must be > 0.",               1.0   ), 
    ( "ImportAsAdvancedMesh",         "boolean",       "Import as Advanced Mesh",   __name__, "Imports meshes using Advanced Mesh when checked or Trimesh when unchecked.", True  ), 
    ( "AutoAdjustRotation",           "boolean",       "Auto-adjust rotation",      __name__, "Account for alternate orientation in modeling programs by rotating the geometry by -90 degrees around the X-axis after import.", 
                                                                                                                                                                            True  )
    # GLS-5991: Unsupported options from the ASE Importer dialog
    #( "ImportVertexColors",           "boolean",       "Import vertex colors ",     __name__, "Imports vertex colors",                                                     True  ), 
    #( "OptimizeAnimations",           "boolean",       "Optimize animations",       __name__, "Optimizes imported animations to reduce keyframes",                         True  ), 
    ]

def Catalyzer_OnScriptAssigned( catalyzerInstance ):
    CatalyzerSourceChangeMonitor.Start( catalyzerInstance, "DSIFile", requireExclusiveLock = True )

def Catalyzer_OnScriptUnassigned( catalyzerInstance ):
    CatalyzerSourceChangeMonitor.Stop( catalyzerInstance, "DSIFile" )
    
def Catalyzer_OnPropertyChanged( catalyzerInstance, propertyName ):
    if "DSIFile" == propertyName:
        CatalyzerSourceChangeMonitor.Start( catalyzerInstance, "DSIFile", requireExclusiveLock = True )
    
def Catalyzer_DoReaction( catalyzerInstance ):
    
    # Get the name of the file we want to import from the object properties
    dsiFileName = catalyzerInstance.GetAttributeValueString( "DSIFile" );

    if not dsiFileName:
        return False;
        
    doc = catalyzerInstance.GetDocument();    
    originalSelection = doc.GetSelectedObjects()
    doc.ClearSelections();
    
    scale = float( catalyzerInstance.GetAttributeValueString( "ScaleFactor" ) )
    if scale <= 0:
        EditorLog.PrintWarning( "Invalid scale (%f) on ASE Importer named '%s'. Forcing to 1." % ( scale, catalyzerInstance.GetName() ) )
        scale = 1.0
        catalyzerInstance.SetAttributeValueString( "ScaleFactor", str( scale ) )
    
    # Note: Do not use backslashes here (even \\escaped ones).  They seem to get interpreted somewhere else in the pipeline.
    importer = GlsASEImporter.GetInstance()  # Get the importer and import the file, which will put a new group at the top level of the document
    newGroup = importer.Import(
        dsiFileName,
        catalyzerInstance.GetDocument(),
        int( catalyzerInstance.GetAttributeValueString( "RepositionOnImport" ) ),  # Reposition
        int( catalyzerInstance.GetAttributeValueString( "EnableLighting"     ) ),  # Enable Lighting
        int( catalyzerInstance.GetAttributeValueString( "SplitAnimations"    ) ),  # Split Animations
        int( catalyzerInstance.GetAttributeValueString( "IgnoreFileNormals"  ) ),  # Ignore File Normals
        scale, # Scale
        False, # Execute Script (GLS-5991: obsolete arg)
        None,  # Script Path,   (GLS-5991: obsolete arg)
        int( catalyzerInstance.GetAttributeValueString( "ImportAsAdvancedMesh" ) ) # Use Advanced Mesh
    )

    CatalyzerSourceChangeMonitor.UpdateFileSignature( catalyzerInstance, "DSIFile" )

    # Move all the objects into the current Catalyzer    
    while newGroup.Count():
        catalyzerInstance.MoveObjectToGroupAllowDuplicateNames( newGroup.GetObjectByIndex(0) )
       
    # Remove the remaining empty group
    newGroup.Ungroup()    
    doc.SelectObjects(originalSelection);  
    
    # Apply the rotation to account for different orientations in modeling programs like 3dsMax
    autoAdjustRotation = int( catalyzerInstance.GetAttributeValueString( "AutoAdjustRotation" ) )
    if autoAdjustRotation:
        EditorLog.PrintInfo( "Rotating %s by -90 degrees around X axis." % catalyzerInstance.GetName() )
        catalyzerInstance.RotateObject( -90.0, catalyzerInstance.X_AXIS )

def Catalyzer_DoMagnify( catalyzerInstance ):	
    print "Got Catalyzer_DoMagnify; opening the 3dsMax file if it is in the same directory ..."
    fileName = os.path.splitext(catalyzerInstance.GetAttributeValue( "DSIFile" ))[0] + ".max"
    os.system('start ' + fileName)
            
def Catalyzer_GetPreferredTypeName():
    return "ASE Import"

