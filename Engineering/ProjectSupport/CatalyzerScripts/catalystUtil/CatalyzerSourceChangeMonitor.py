# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import Document
import ExtendedFileMonitor
import FileSignatureSupport
import weakref

_fileMon = ExtendedFileMonitor.GetFileMonitor()
   
# Stops watching for changes to the file referred to in the specified attribute
def Stop( catalyzerInstance, fileNameAttributeName ):
    idAttrName = _IdName( fileNameAttributeName )
    if hasattr(catalyzerInstance, idAttrName):
        _fileMon.Unregister( getattr(catalyzerInstance, idAttrName) )
        setattr( catalyzerInstance, idAttrName, 0 )
        
# Starts watching for changes to the file referred to in the specified attribute.
# If it finds a change, it will call catalyzerInstance.SetNeedsReaction() which
# will eventually cause the Catalyzer to run again (e.g., to re-import the file).
# - Each time Start() is called, it will first stop watching any previous file.
# - Exclusive locking is useful for exporters (such as the GLS 3ds Max DSI Exporter)
#   that take more than a second to complete writing out the file that is being 
#   monitored. The file monitor will only trigger the OnChanged() callback when the
#   export is complete and the other program has closed the file. 
# - Exclusive locking should generally not be used when the file being monitored
#   by GL Studio is the same one held open in an external editor because GL Studio
#   won't be able to get an exclusive lock and so won't fire the OnChanged() callback
#   (which means that a Catalyzer like the PSD Importer won't re-import automatically).
def Start( catalyzerInstance, fileNameAttributeName, requireExclusiveLock = False ):
    idAttrName = _IdName( fileNameAttributeName )
    
    originalFileName = catalyzerInstance.GetAttributeValueString( fileNameAttributeName )
    
    if not hasattr(catalyzerInstance, idAttrName):
        setattr( catalyzerInstance, idAttrName, None )

    # Un-watch any existing file
    Stop( catalyzerInstance, fileNameAttributeName )
    
    if not originalFileName:
        return # No need to watch for an empty file to ever exist
        
    class local:
        weakInstanceRef = None
        originalFileName = ""
    local.weakInstanceRef = weakref.ref( catalyzerInstance ) 
    local.originalFileName = originalFileName
    
    def OnChanged(id, changeType):
        instance = local.weakInstanceRef()
        # Do some sanity checks to see if this object is still valid
        if ( instance and 
             not instance.isNULL() and
             instance.GetDocument() and 
             instance.GetParent() and 
             local.originalFileName == instance.GetAttributeValueString( fileNameAttributeName )
            ):
            if not changeType == 1:
                if FileSignatureSupport.FileSignatureChanged( catalyzerInstance, fileNameAttributeName ):
                    instance.SetNeedsReaction()                    
            
    setattr( catalyzerInstance, idAttrName, _fileMon.Register( originalFileName, OnChanged, requireExclusiveLock ) )

# Updates the file signature stored in the specified catalyzerInstance
def UpdateFileSignature( catalyzerInstance, fileNameAttributeName ):
    print "Updating file signature for ", catalyzerInstance.GetName(), "  ", fileNameAttributeName
    FileSignatureSupport.UpdateFileSignature( catalyzerInstance, fileNameAttributeName )
    
# The attribute name that is dynamically added to the catalyzerInstance
def _IdName( fileNameAttributeName ):
    return "_sourceChangeMonitorFor_"+fileNameAttributeName   
    
      