# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" File signature support utilities for tracking file changes on disk """

import os
import zlib
import ast

def GetFastSignature( fileName ):
    return str( os.path.getmtime(fileName) )
    
def crc( fileName ):
    prev = 0
    for eachLine in open(fileName,"rb"):
        prev = zlib.crc32(eachLine, prev)
    return "%X"%(prev & 0xFFFFFFFF)
    
def GetDeepSignature( fileName ):
    return crc( fileName )
   

# Cache across all of the GL Studio Editor
_deepSignatureCache = {}
def _SaveCachedDeepSignature( fileName, fast, deep ):
    # print "Saving signature for ", fileName, " fast: ", fast, " deep: ", deep
    _deepSignatureCache[ os.path.abspath(fileName)+fast ] = deep
    
def _GetCachedDeepSignature( fileName, fast ):
    keyName = os.path.abspath(fileName)+fast
    if keyName in _deepSignatureCache:
        # print "Got cache hit with ", fileName
        return _deepSignatureCache[ keyName ]
    return None
    
def FileSignatureChanged( catalyzerInstance, fileAttributeName ):
    fileName = catalyzerInstance.GetAttributeValueString( fileAttributeName );
    previousSignatureString = catalyzerInstance.GetAttributeValueString( "_"+fileAttributeName+"_LastUpdateSignature" )
    try:
        previousSignatureDict = ast.literal_eval(previousSignatureString)
        fastSignature = GetFastSignature( fileName )
        if previousSignatureDict['fast'] == fastSignature:
            # The fast check matches so we don't have to go any further
            return False
        else:
            deepSignature = _GetCachedDeepSignature( fileName, fastSignature )
            if not deepSignature:
                deepSignature = GetDeepSignature( fileName )
                _SaveCachedDeepSignature( fileName, fastSignature, deepSignature )
                
            if previousSignatureDict['deep'] == deepSignature:
                # Since the deep signature matches, we will ignore the 'fast' mismatch.
                # This could easily happen after checking files out from revision control
                return False
    except:
        # print "Got exception in FileSignatureChanged()"
        pass
        
    return True
    
def UpdateFileSignature( catalyzerInstance, fileAttributeName ):
    fileName = catalyzerInstance.GetAttributeValueString( fileAttributeName );
    fast = GetFastSignature( fileName )
    deep = _GetCachedDeepSignature( fileName, fast )
    if not deep:
        deep = GetDeepSignature( fileName )
        _SaveCachedDeepSignature( fileName, fast, deep )       
    newSignature = str({'fast':fast, 'deep':deep})
    # print 'new signature is now: ', newSignature
    catalyzerInstance.SetAttributeValueString( "_"+fileAttributeName+"_LastUpdateSignature", newSignature )
    # GLS-5692	Updating catalyzer should dirty the file save flag.  Without this, the user can't save anything about 
    # the import without modifying something else.  This will mean that each time the .gls file gets loaded, it will re-import.
    
