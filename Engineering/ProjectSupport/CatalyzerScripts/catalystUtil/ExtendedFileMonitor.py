# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Extensions to the GL Studio FileMonitor class """

import threading
import FileMonitor
import Editor

# Utility
def CallOnMainThreadAfter( seconds, func, *args, **keywords):
    def callIt():
        func(*args, **keywords)
    def timerCallback():
        if not Editor.shuttingDownEvent.isSet():
            Editor.mainThreadQueue.put(callIt) 
    t = threading.Timer( seconds , timerCallback )
    t.daemon = True
    t.start() 
    

# This FileMonitor class wraps the FileMonitor class to provide extended file watching.
# Specifically, it never gives up looking for a file.
# Once you tell it to look for a file, it will keep looking for it and reporting status changes
# until told to stop.    
class ExtendedFileMonitor:
    def __init__( self ):
        self._realFileMon = FileMonitor.GetFileMonitor()
        self._currentMyId = 0
        self._myIdToRealId = {}
    
    def _DoRegister( self, myId, fileName, func, requireExclusiveLock ):
        class local:
            myId = None
        local.myId = myId 
        def idChangingFunc( realId, changeType ):
            func( local.myId, changeType );
        newId = self._realFileMon.Register( fileName, idChangingFunc, requireExclusiveLock )
        if not newId:
            # Spawn a thread to keep trying
            # Keep track of the fact that we are trying to load this id
            self._myIdToRealId[self._currentMyId] = 0
            def tryAgain():
                if local.myId in self._myIdToRealId:            
                    newId = self._realFileMon.Register( fileName, idChangingFunc, requireExclusiveLock )
                    if not newId:                    
                        #print 'Setting up to wait to try again for: ', fileName
                        CallOnMainThreadAfter( 2, tryAgain )           
                    else:
                        #print 'Successfully monitoring ',fileName, 'with id: ',newId
                        self._myIdToRealId[local.myId] = newId
                        # Tell the registrar that the file has appeared.
                        func( local.myId, 2 )
            tryAgain()            
        else:
            #print 'Successfully monitoring ',fileName, 'with id: ',newId
            self._myIdToRealId[self._currentMyId] = newId
            # Tell the registrar that the file has appeared.
            func( local.myId, 2 )
        
        
    def Register( self, fileName, func, requireExclusiveLock ):
        self._currentMyId += 1
        
        def myFunc(id, changeType):
            func(id, changeType)
            if 1 == changeType:
                # Deleted, so re-add.
                # The _realFileMon unregisters itself whenever a file is deleted
                # but the ExtendedFileMonitor continues to watch, so we have to 
                # keep trying to re-register which _DoRegister will do for us.
                self._DoRegister( self._currentMyId, fileName, myFunc, requireExclusiveLock )       
                
        self._DoRegister( self._currentMyId, fileName, myFunc, requireExclusiveLock )       
            
        return self._currentMyId
            
    def Unregister( self, myId ):
        if myId in self._myIdToRealId:            
            # Delete the myId from the dictionary
            realId = self._myIdToRealId.pop( myId )
            self._realFileMon.Unregister( realId )
            
    _instance = None
    
def GetFileMonitor():
    if not ExtendedFileMonitor._instance:
        ExtendedFileMonitor._instance = ExtendedFileMonitor();
    return ExtendedFileMonitor._instance    