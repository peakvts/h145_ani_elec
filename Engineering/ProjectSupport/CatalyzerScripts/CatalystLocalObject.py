# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" Catalyst - References a local object in the same GLS file

NOTE: There is currently no mechanism to automatically update the reference on change.
"""

def Catalyzer_GetPropertiesList():
    # [ ( internalName, propType, displayName, groupName, description, defaultValue ), ]
    return [( "ReferencedObjects", "ascii", "Referenced Objects", __name__, "All the object to copy into the catalyzer seperated by spaces", "" ), ]

#Utilities

def _SelectIdentifiedObjects( catalyzerInstance ):
    doc = catalyzerInstance.GetDocument()
    objectNames = catalyzerInstance.GetAttributeValueString( "ReferencedObjects" )
    doc.ClearSelections()

    someSelected = False
    objNamesList = objectNames.split()
    for name in objNamesList:
        obj = doc.GetObjectByName(name)
        if obj:
            doc.SelectObject(obj)
            someSelected = True

    return someSelected
    
       
def Catalyzer_DoReaction( catalyzerInstance ):
    doc = catalyzerInstance.GetDocument()
    originalSelection = doc.GetSelectedObjects()

    someSelected = _SelectIdentifiedObjects( catalyzerInstance )
        
    if someSelected:
        doc.CopySelectedObjects()
        doc.ClearSelections()
        doc.PasteClipboardObjects( True, True )
        # The new copy is now selected

        newSelectedObjects = doc.GetSelectedObjects()
        for i in range(newSelectedObjects.GetCount()):
            catalyzerInstance.MoveObjectToGroupAllowDuplicateNames(newSelectedObjects.GetObjectByIndex(i))
            
    # Need to clear selections, otherwise if the select objects call below has no objects to restore, it will leave the current selection as
    # is, which contains the objects we just moved out of the selection above.  Which will cause a crash the next time this script is run.
    doc.ClearSelections();       
    # Put the selection back the way we found it
    doc.SelectObjects(originalSelection);
    
def Catalyzer_GetPreferredTypeName( ):
    return "LocalRef"
    
def Catalyzer_DoMagnify( catalyzerInstance ):	
    # We are going to select our local object(s) in the hierarchy.
    _SelectIdentifiedObjects( catalyzerInstance )
    

