# This file is licensed under the MIT License.
# 
# Copyright ( c ) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files ( the "Software" ), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" GLS Action - Update main eyePoint for use in ES / WebGL. Create one if there is not already one defined. """

import Document
import GlsEyePoint
import Editor
import EditorLog
import ProjectConfiguration
import Vector

doc = Editor.GetCurrentDocument()
if not doc:
    EditorLog.PrintError( "Unable to update the initial eye point because no document is open." );
    return
    
# Use the default eyepoint unless the document has an eye point set up
eyePointName = ProjectConfiguration.current.defaultEyePointName
if doc.GetProjectionMode() == doc.PROJECTION_MODE_EYEPOINT:
    eyePointName = doc.GetInitialEyePointName()

# Check if the eye point itself actually exists
eyePoint = doc.GetObjectByName( eyePointName )
if not eyePoint:
    eyePoint = GlsEyePoint.GlsEyePoint()
    eyePoint.SetName( eyePointName )
    doc.InsertObject( eyePoint )
else:
    eyePoint = GlsEyePoint.CastToGlsEyePoint( eyePoint )
    if eyePoint.isNULL():
        EditorLog.PrintError( "Unable to update the initial eye point because an object named %s exists but is not a GlsEyePoint." % ProjectConfiguration.current.defaultEyePointName )
        return

# Configure the eye point based on our canvas size
width    = doc.GetInitialWidth()
height   = doc.GetInitialHeight()
location = Vector.Vector( width / 2, height / 2, 1000 )

eyePoint.Orthographic( True )
eyePoint.FovIsHorizontal( True )
eyePoint.OrthoSize( width ) # horizontal
eyePoint.OtherOrthoSize( height ) # vertical
eyePoint.HorizontalConstraint( eyePoint.FOV_CONSTRAINT_EXACTLY )
eyePoint.VerticalConstraint(   eyePoint.FOV_CONSTRAINT_EXACTLY )
eyePoint.SetLocation( location )

# Set it as the eye point for the document
doc.SetInitialEyePointName( eyePointName )
doc.SetProjectionMode( doc.PROJECTION_MODE_EYEPOINT )
doc.SetRedrawMode( doc.REDRAW_MODE_CONTINUOUS )
