# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" GLS Action - Sets up drag-drop handlers for this project."""

import Editor
import EditorLog
import Catalyzer
import Document
import sys
import os
import ProjectConfiguration

sys.path.append( ProjectConfiguration.current.catalyzerScriptsDir )


def Setup():
    #######################################################################
    import CreateNineDotPngPoly
    
    def CanDropAsNineDotPngPoly( fileName ):
        return fileName.lower().endswith(".9.png")

    def CreateDotNineDotPngNinePatch( fileName ):
        if not CanDropAsNineDotPngPoly( fileName ):
            return False
        return CreateNineDotPngPoly.CreateNinePatchFromFile( fileName )

    if not Editor.RegisterDragAndDropHandlers( CanDropAsNineDotPngPoly, CreateDotNineDotPngNinePatch ):
        EditorLog.PrintWarning( "Unable to register 9.png Drag-n-Drop handler." )

    ########################################################################################################    
    import CreatePolygonFromImage

    def CanDropAsTextureSizedPoly( fileName ):
        return any( fileName.lower().endswith(x) for x in ( ".png", ".jpg", ".jpeg", ".gif", ".bmp", ".tga", ".tif", ".tiff", ".xbm" ) )
        
    def CreateTextureSizedPoly( fileName ):
        if not CanDropAsTextureSizedPoly( fileName ):
            return False
        return CreatePolygonFromImage.CreateObjectFromFile( fileName )

    if not Editor.RegisterDragAndDropHandlers( CanDropAsTextureSizedPoly, CreateTextureSizedPoly ):
        EditorLog.PrintWarning( "Unable to register Texture-sized Poly Drag-n-Drop handler." )

    ########################################################################################################    

    def CanDropAsPSDReferenceObject( fileName ):
        return os.path.splitext(fileName)[1].lower() == ".psd"

    def CreatePSDReferenceObject( fileName ):
        if CanDropAsPSDReferenceObject( fileName ):
            doc = Document.GetCurrentDocument()
            if doc:
                catalyzer = Catalyzer.Catalyzer()
                try:
                    objectName = os.path.basename( os.path.splitext(fileName)[0] )
                    catalyzer.SetName( objectName )
                except:
                    catalyzer.SetName("psdReference")
                doc.InsertObject( catalyzer, True )
                catalyzer.AddScript( ProjectConfiguration.current.RelativeToDoc( doc, 'catalyzerScriptsDir', 'CatalystPSDImport.py' ) )
                catalyzer.SetAttributeValueString("PSDFile", fileName)
                return True
        return False   

    if not Editor.RegisterDragAndDropHandlers( CanDropAsPSDReferenceObject, CreatePSDReferenceObject ):
        EditorLog.PrintWarning( "Unable to register PSD Reference Drag-n-Drop handler." )

    ########################################################################################################    

    def CanDropASEReferenceObject( fileName ):
        return os.path.splitext(fileName)[1].lower() in ( ".ase", ".dsi" )

    def CreateASEReferenceObject( fileName ):
        if CanDropASEReferenceObject( fileName ):
            doc = Document.GetCurrentDocument()
            if doc:
                catalyzer = Catalyzer.Catalyzer()
                try:
                    objectName = os.path.basename(os.path.splitext(fileName)[0])
                    catalyzer.SetName(objectName)
                except:
                    catalyzer.SetName("aseReference")
                doc.InsertObject(catalyzer, True)
                catalyzer.AddScript( ProjectConfiguration.current.RelativeToDoc( doc, 'catalyzerScriptsDir', 'CatalystASEImport.py' ) )
                catalyzer.SetAttributeValueString("DSIFile", fileName)
                return True
        return False   

    if not Editor.RegisterDragAndDropHandlers( CanDropASEReferenceObject, CreateASEReferenceObject ):
        EditorLog.PrintWarning( "Unable to register ASE Reference Drag-n-Drop handler." )
