# This file is licensed under the MIT License.
# 
# Copyright (c) 2015 by The DiSTI Corporation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

""" GLS Action - Copies data describing the current selection for use by the corresponding paste action
    Stores the data in a made-up attribte: Editor.externalReferenceCopyData
"""
import Editor
import EditorLog
import Document
import os

docToReference = Document.GetCurrentDocument()
if docToReference:  
    filenameToReference = os.path.join(docToReference.GetDirname(), docToReference.GetFilename())     

    selectedObjects = docToReference.GetSelectedObjects()
    selectedObjectString = ""
    for i in range(selectedObjects.GetCount()):
        selectedObjectString += selectedObjects.GetObjectByIndex(i).GetName() + " "
        
    Editor.externalReferenceCopyData = ( filenameToReference, selectedObjectString )
    EditorLog.PrintInfo("Reference copied.  Now paste the external reference into the desired location.")
                         
