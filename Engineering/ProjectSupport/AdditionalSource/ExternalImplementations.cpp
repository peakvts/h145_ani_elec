#ifdef GLES
  // For ES (with WebGL) we build components
  #include "FrontPage.h"
#else
  // For Desktop we build applications
  #include "FrontPageApp.h"  
#endif

using namespace disti;
//////////////////////////////////////////////////////////////
// If a method's "Generate Method Body" is unchecked, 
// you can define the method bodies here (or in other seperate files).
//////////////////////////////////////////////////////////////

// Example method body:
/*
void FrontPageClass::Calculate (double time)
{
    objects->Group::Calculate(time);
}
*/
